package com.dags.ecommerceitem.api.mapper

import com.dags.ecommerceitem.api.request.ItemForm
import com.dags.ecommerceitem.api.request.UpdateItemForm
import com.dags.ecommerceitem.api.response.ItemApiResp
import com.dags.ecommerceitem.api.response.ItemResource
import com.dags.ecommerceitem.api.response.UserResource
import com.dags.ecommerceitem.item.domain.model.Item
import com.dags.ecommerceitem.item.domain.param.item.ItemParam
import com.dags.ecommerceitem.item.domain.param.item.UpdateItemParam
import com.dags.ecommerceitem.user.domain.model.User

interface ItemApiMapper {
    fun map(item: List<Item>): List<ItemResource>
    fun map(item: Item): ItemResource
    fun map(user: User): UserResource
    fun map(form: ItemForm): Item
}