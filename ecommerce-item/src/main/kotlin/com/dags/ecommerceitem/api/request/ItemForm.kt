package com.dags.ecommerceitem.api.request

import com.fasterxml.jackson.annotation.JsonProperty

data class ItemForm(
        @JsonProperty("userId")
        var userId: String,

        @JsonProperty("name")
        var name: String
)
