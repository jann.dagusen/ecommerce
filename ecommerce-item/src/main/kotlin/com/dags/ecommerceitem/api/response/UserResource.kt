package com.dags.ecommerceitem.api.response

data class UserResource(
        var userId: String? = null,
        var email: String? = null
)