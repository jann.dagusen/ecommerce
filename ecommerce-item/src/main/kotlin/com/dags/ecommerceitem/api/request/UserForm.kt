package com.dags.ecommerceitem.api.request

data class UserForm(
        var userId: String? = null,
        var username: String? = null
)