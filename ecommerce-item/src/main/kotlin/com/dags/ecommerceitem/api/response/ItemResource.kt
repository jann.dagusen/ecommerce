package com.dags.ecommerceitem.api.response

data class ItemResource(
        val id: Long?,
        val user: UserResource?,
        val name: String
)