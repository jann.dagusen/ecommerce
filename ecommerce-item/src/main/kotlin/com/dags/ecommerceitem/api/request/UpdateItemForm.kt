package com.dags.ecommerceitem.api.request

import com.fasterxml.jackson.annotation.JsonProperty

data class UpdateItemForm(
        @JsonProperty
        val id: Long,

        @JsonProperty("name")
        val name: String
)