package com.dags.ecommerceitem.api.mapper.impl

import com.dags.ecommerceitem.api.mapper.ItemApiMapper
import com.dags.ecommerceitem.api.request.ItemForm
import com.dags.ecommerceitem.api.request.UpdateItemForm
import com.dags.ecommerceitem.api.response.ItemApiResp
import com.dags.ecommerceitem.api.response.ItemResource
import com.dags.ecommerceitem.api.response.UserResource
import com.dags.ecommerceitem.item.domain.model.Item
import com.dags.ecommerceitem.item.domain.param.item.ItemParam
import com.dags.ecommerceitem.item.domain.param.item.UpdateItemParam
import com.dags.ecommerceitem.user.domain.model.User
import org.springframework.stereotype.Service

@Service
class ItemApiMapperImpl : ItemApiMapper {
    override fun map(item: List<Item>): List<ItemResource> {
        return item.map {
            map(it)
        }
    }

    override fun map(item: Item): ItemResource {
        return ItemResource(
                item.id,
                map(item.user),
                item.name
        )
    }

    override fun map(user: User): UserResource {
        return UserResource(
                user.userId,
                user.email
        )
    }

    override fun map(form: ItemForm): Item {
        return Item(
                null,
                User(form.userId, ""),
                form.name
        )
    }
}