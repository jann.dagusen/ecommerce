package com.dags.ecommerceitem.api.response

import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

data class ItemApiResp(
        @JsonProperty("id")
        val id: Long,

        @JsonProperty("userId")
        val userId: String,

        @JsonProperty("name")
        val name: String,

        @JsonProperty("createdAt")
        val createdAt: Date
)