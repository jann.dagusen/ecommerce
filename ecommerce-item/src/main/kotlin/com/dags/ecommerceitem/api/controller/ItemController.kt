package com.dags.ecommerceitem.api.controller

import com.dags.ecommerceitem.api.mapper.ItemApiMapper
import com.dags.ecommerceitem.api.request.ItemForm
import com.dags.ecommerceitem.api.response.ItemApiResp
import com.dags.ecommerceitem.api.response.ItemResource
import com.dags.ecommerceitem.item.domain.interactor.item.CreateItem
import com.dags.ecommerceitem.item.domain.interactor.item.DeleteItem
import com.dags.ecommerceitem.item.domain.interactor.item.GetItems
import com.dags.ecommerceitem.item.domain.interactor.item.UpdateItem
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("items")
class ItemController @Autowired constructor(val mapper: ItemApiMapper,
                                            val getItems: GetItems,
                                            val createItem: CreateItem,
                                            val updateItem: UpdateItem,
                                            val deleteItem: DeleteItem) {
    @GetMapping
    fun index(@RequestParam userId: String?): List<ItemResource> {
        return mapper.map(getItems.execute(userId))
    }

    @PostMapping
    fun create(@RequestBody form: ItemForm): ItemResource = mapper.map(createItem.execute(mapper.map(form)))

//    @PatchMapping("/update-item")
//    fun update(@RequestBody editItem: UpdateItemForm): ItemApiResp {
//        return mapper.map(updateItem.execute(mapper.map(editItem)))
//    }
//
//    @DeleteMapping("/{id}")
//    fun delete(@PathVariable id: Long): ItemApiResp = mapper.map(deleteItem.execute(id))
}