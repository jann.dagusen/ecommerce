package com.dags.ecommerceitem.item.data.entity

import java.util.*
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "items")
data class ItemEntity(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        val id: Long?,

        @Column(name = "user_id")
        var userId: String?,

        @Column(name = "name")
        var name: String
) {
    constructor(userId: String?, name: String) : this(null, userId!!, name)
}