package com.dags.ecommerceitem.item.data.gateway.item

import com.dags.ecommerceitem.item.data.mapper.item.ItemDataMapper
import com.dags.ecommerceitem.item.data.repository.item.ItemRepository
import com.dags.ecommerceitem.item.domain.gateway.item.UpdateItemGateway
import com.dags.ecommerceitem.item.domain.model.Item
import com.dags.ecommerceitem.item.domain.param.item.UpdateItemParam
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UpdateItemGatewayImpl @Autowired constructor(val mapper: ItemDataMapper,
                                                   val repository: ItemRepository) : UpdateItemGateway {
    override fun update(item: UpdateItemParam): Item {
        val currItem = repository.findById(item.id)
        val newItem = repository.save(currItem
                .get()
                .apply {
                    name = item.name
                })
        return mapper.map(newItem)
    }
}