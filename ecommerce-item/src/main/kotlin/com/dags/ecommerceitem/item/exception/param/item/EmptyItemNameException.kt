package com.dags.ecommerceitem.item.exception.param.item

import com.dags.ecommerceitem.item.exception.param.EmptyStringException

class EmptyItemNameException : EmptyStringException("Item Name")