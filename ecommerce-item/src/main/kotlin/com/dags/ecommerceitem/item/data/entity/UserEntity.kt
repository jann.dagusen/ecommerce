package com.dags.ecommerceitem.item.data.entity

class UserEntity(
        var userId: String,
        var name: String
)