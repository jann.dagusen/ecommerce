package com.dags.ecommerceitem.item.data.gateway.item

import com.dags.ecommerceitem.item.data.mapper.item.ItemDataMapper
import com.dags.ecommerceitem.item.data.repository.item.ItemRepository
import com.dags.ecommerceitem.item.domain.gateway.item.FetchItemsGateway
import com.dags.ecommerceitem.item.domain.model.Item
import com.dags.ecommerceitem.user.domain.gateway.FetchUserGateway
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class FetchItemsGatewayImpl @Autowired constructor(val mapper: ItemDataMapper,
                                                   val repository: ItemRepository,
                                                   val fetchUserGateway: FetchUserGateway) : FetchItemsGateway {
    override fun findAll(userId: String?): List<Item> {
//        if (userId.isNullOrBlank()) {
//            return mapper.map(repository.findAll())
//        }
        val item: List<Item> = mapper.map(repository.findAll())
        val ids = HashSet<String>()
        item.forEach {
            ids.add(it.user.userId)
        }
//        val users = fetchUserGateway.find(ids.toList())
//        item.map {
//            it.user.apply {
//                users!![this.userId]!!.forEach {
//                    this.email = it.email
//                }
//            }
//        }
        return item
    }

}