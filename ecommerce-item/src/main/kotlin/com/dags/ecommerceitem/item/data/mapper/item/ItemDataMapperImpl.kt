package com.dags.ecommerceitem.item.data.mapper.item

import com.dags.ecommerceitem.item.data.entity.ItemEntity
import com.dags.ecommerceitem.item.data.entity.UserEntity
import com.dags.ecommerceitem.item.domain.model.Item
import com.dags.ecommerceitem.user.domain.model.User
import org.springframework.stereotype.Service

@Service
class ItemDataMapperImpl : ItemDataMapper {
    override fun map(entities: List<ItemEntity>): List<Item> = entities.map { map(it) }

    override fun map(entity: ItemEntity): Item = Item(
            entity.id!!,
            map(UserEntity(
                    entity.userId!!,
                    ""
            )),
            entity.name
    )

    override fun map(user: UserEntity): User = User(
            user.userId,
            user.name
    )
}