package com.dags.ecommerceitem.item.exception.param

import com.dags.ecommerceitem.item.exception.ParamException

abstract class EmptyStringException(field: String) : ParamException("$field cannot be empty!")