package com.dags.ecommerceitem.item.domain.interactor.item.impl

import com.dags.ecommerceitem.item.data.gateway.item.FetchItemsGatewayImpl
import com.dags.ecommerceitem.item.domain.interactor.item.GetItems
import com.dags.ecommerceitem.item.domain.model.Item
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class GetItemsImpl @Autowired constructor(val gateway: FetchItemsGatewayImpl) : GetItems {
    override fun execute(userId: String?): List<Item> = gateway.findAll(userId)
}