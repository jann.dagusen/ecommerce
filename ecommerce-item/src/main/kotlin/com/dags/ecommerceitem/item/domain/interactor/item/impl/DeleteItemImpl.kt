package com.dags.ecommerceitem.item.domain.interactor.item.impl

import com.dags.ecommerceitem.item.data.gateway.item.DeleteItemGatewayImpl
import com.dags.ecommerceitem.item.domain.interactor.item.DeleteItem
import com.dags.ecommerceitem.item.domain.model.Item
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class DeleteItemImpl @Autowired constructor(val gateway: DeleteItemGatewayImpl) : DeleteItem {
    override fun execute(id: Long): Item {
        return gateway.remove(id)
    }
}