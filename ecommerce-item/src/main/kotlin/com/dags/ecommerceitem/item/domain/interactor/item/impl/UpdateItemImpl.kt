package com.dags.ecommerceitem.item.domain.interactor.item.impl

import com.dags.ecommerceitem.item.data.gateway.item.UpdateItemGatewayImpl
import com.dags.ecommerceitem.item.domain.interactor.item.UpdateItem
import com.dags.ecommerceitem.item.domain.model.Item
import com.dags.ecommerceitem.item.domain.param.item.UpdateItemParam
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UpdateItemImpl @Autowired constructor(val gateway: UpdateItemGatewayImpl) : UpdateItem {
    override fun execute(item: UpdateItemParam): Item {
        return gateway.update(item)
    }
}