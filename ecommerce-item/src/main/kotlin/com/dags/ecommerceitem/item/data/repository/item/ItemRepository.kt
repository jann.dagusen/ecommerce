package com.dags.ecommerceitem.item.data.repository.item

import com.dags.ecommerceitem.item.data.entity.ItemEntity
import org.springframework.data.jpa.repository.JpaRepository

interface ItemRepository : JpaRepository<ItemEntity, Long> {
    fun findAllByNameContains(q: String): List<ItemEntity>
    fun findAllByUserId(userId: String?): List<ItemEntity>
}