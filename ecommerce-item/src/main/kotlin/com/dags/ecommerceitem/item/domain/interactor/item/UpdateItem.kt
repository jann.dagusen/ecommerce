package com.dags.ecommerceitem.item.domain.interactor.item

import com.dags.ecommerceitem.item.domain.model.Item
import com.dags.ecommerceitem.item.domain.param.item.UpdateItemParam

interface UpdateItem {
    fun execute(item: UpdateItemParam): Item
}