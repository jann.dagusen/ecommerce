package com.dags.ecommerceitem.item.domain.interactor.item.impl

import com.dags.ecommerceitem.item.data.gateway.item.CreateItemGatewayImpl
import com.dags.ecommerceitem.item.domain.interactor.item.CreateItem
import com.dags.ecommerceitem.item.domain.model.Item
import com.dags.ecommerceitem.item.domain.param.item.ItemParam
import com.dags.ecommerceitem.item.exception.param.item.EmptyItemNameException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CreateItemImpl @Autowired constructor(val gateway: CreateItemGatewayImpl) : CreateItem {
    override fun execute(item: Item): Item {
        if (item.name.isEmpty()) throw EmptyItemNameException()

        return gateway.create(item)
    }
}