package com.dags.ecommerceitem.item.domain.interactor.item

import com.dags.ecommerceitem.item.domain.model.Item

interface GetItems {
    fun execute(userId: String?): List<Item>
}
