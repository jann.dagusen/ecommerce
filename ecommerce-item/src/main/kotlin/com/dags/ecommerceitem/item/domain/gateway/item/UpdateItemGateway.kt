package com.dags.ecommerceitem.item.domain.gateway.item

import com.dags.ecommerceitem.item.domain.model.Item
import com.dags.ecommerceitem.item.domain.param.item.UpdateItemParam

interface UpdateItemGateway {
    fun update(item: UpdateItemParam): Item
}