package com.dags.ecommerceitem.item.domain.param.item

data class ItemParam(
        val userId: String,
        val name: String
)