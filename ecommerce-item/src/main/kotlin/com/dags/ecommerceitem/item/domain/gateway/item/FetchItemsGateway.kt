package com.dags.ecommerceitem.item.domain.gateway.item

import com.dags.ecommerceitem.item.domain.model.Item

interface FetchItemsGateway {
    fun findAll(userId: String?): List<Item>
}