package com.dags.ecommerceitem.item.exception

abstract class ParamException(message: String) : Exception(message)