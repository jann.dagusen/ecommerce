package com.dags.ecommerceitem.item.domain.model

import com.dags.ecommerceitem.user.domain.model.User

data class Item(
        val id: Long?,
        val user: User,
        val name: String
)