package com.dags.ecommerceitem.item.domain.interactor.item

import com.dags.ecommerceitem.item.domain.model.Item

interface DeleteItem {
    fun execute(id: Long): Item
}