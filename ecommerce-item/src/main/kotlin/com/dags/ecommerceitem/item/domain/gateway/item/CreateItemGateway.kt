package com.dags.ecommerceitem.item.domain.gateway.item

import com.dags.ecommerceitem.item.domain.model.Item
import com.dags.ecommerceitem.item.domain.param.item.ItemParam

interface CreateItemGateway {
    fun create(item: Item): Item
}