package com.dags.ecommerceitem.item.domain.gateway.item

import com.dags.ecommerceitem.item.domain.model.Item

interface DeleteItemGateway {
    fun remove(id: Long): Item
}