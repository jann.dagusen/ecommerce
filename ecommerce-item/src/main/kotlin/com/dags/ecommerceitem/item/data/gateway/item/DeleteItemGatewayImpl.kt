package com.dags.ecommerceitem.item.data.gateway.item

import com.dags.ecommerceitem.item.data.mapper.item.ItemDataMapper
import com.dags.ecommerceitem.item.data.repository.item.ItemRepository
import com.dags.ecommerceitem.item.domain.gateway.item.DeleteItemGateway
import com.dags.ecommerceitem.item.domain.model.Item
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class DeleteItemGatewayImpl @Autowired constructor(val mapper: ItemDataMapper,
                                                   val repository: ItemRepository) : DeleteItemGateway {
    override fun remove(id: Long): Item {
        val itemToBeDeleted = repository.findById(id).get()
        repository.deleteById(id)
        return mapper.map(itemToBeDeleted)
    }
}