package com.dags.ecommerceitem.item.data.gateway.item

import com.dags.ecommerceitem.item.data.entity.ItemEntity
import com.dags.ecommerceitem.item.data.mapper.item.ItemDataMapper
import com.dags.ecommerceitem.item.data.repository.item.ItemRepository
import com.dags.ecommerceitem.item.domain.gateway.item.CreateItemGateway
import com.dags.ecommerceitem.item.domain.model.Item
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CreateItemGatewayImpl @Autowired constructor(val mapper: ItemDataMapper,
                                                   val repository: ItemRepository) : CreateItemGateway {
    override fun create(item: Item): Item {
        return mapper.map(repository.save(ItemEntity(
                item.name,
                item.user.userId
        )))
    }
}