package com.dags.ecommerceitem.item.domain.param.item

data class UpdateItemParam(
        val id: Long,
        val name: String
)