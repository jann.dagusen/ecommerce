package com.dags.ecommerceitem.item.data.mapper.item

import com.dags.ecommerceitem.item.data.entity.ItemEntity
import com.dags.ecommerceitem.item.data.entity.UserEntity
import com.dags.ecommerceitem.item.domain.model.Item
import com.dags.ecommerceitem.user.domain.model.User


interface ItemDataMapper {
    fun map(entities: List<ItemEntity>) : List<Item>
    fun map(entity: ItemEntity): Item
    fun map(user: UserEntity): User
}