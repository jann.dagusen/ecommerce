package com.dags.ecommerceitem

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class EcommerceItemApplication

fun main(args: Array<String>) {
	runApplication<EcommerceItemApplication>(*args)
}
