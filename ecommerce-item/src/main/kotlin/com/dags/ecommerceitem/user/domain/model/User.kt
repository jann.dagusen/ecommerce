package com.dags.ecommerceitem.user.domain.model

data class User(

        var userId: String,
        var email: String
)