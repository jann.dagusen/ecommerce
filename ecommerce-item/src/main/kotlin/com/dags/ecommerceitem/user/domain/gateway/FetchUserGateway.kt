package com.dags.ecommerceitem.user.domain.gateway

import com.dags.ecommerceitem.user.domain.model.User

interface FetchUserGateway {
    fun find(ids:List<String>): Map<String, List<User>>??
}