package com.dags.ecommerceusers.api.response

import com.fasterxml.jackson.annotation.JsonProperty

data class UserApiResp(
        @JsonProperty("id")
        val id: Long,

        @JsonProperty("username")
        val username: String,

        @JsonProperty("password")
        val password: String
)