package com.dags.ecommerceusers.api.request

import com.fasterxml.jackson.annotation.JsonProperty

data class UserForm(
        @JsonProperty("username")
        var username: String,

        @JsonProperty("password")
        var password: String
)