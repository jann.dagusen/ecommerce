package com.dags.ecommerceusers.api.controller

import com.dags.ecommerceusers.api.mapper.UserApiMapper
import com.dags.ecommerceusers.api.request.UserForm
import com.dags.ecommerceusers.api.response.UserApiResp
import com.dags.ecommerceusers.users.domain.interactor.user.CreateUser
import com.dags.ecommerceusers.users.domain.interactor.user.GetUser
import com.dags.ecommerceusers.users.domain.interactor.user.GetUsers
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("users")
class UserController @Autowired constructor(val mapper: UserApiMapper,
                                            val getUsers: GetUsers,
                                            val createUser: CreateUser,
                                            val getUser: GetUser) {
    @GetMapping
    fun index(@RequestParam name: String?): List<UserApiResp> = mapper.map(getUsers.execute(name))

    @PostMapping("/add")
    fun create(@RequestBody form: UserForm): UserApiResp = mapper.map(createUser.execute(mapper.map(form)))

    @GetMapping("/{id}")
    fun getUser(@PathVariable id: Long) : UserApiResp = mapper.map(getUser.execute(id))
}