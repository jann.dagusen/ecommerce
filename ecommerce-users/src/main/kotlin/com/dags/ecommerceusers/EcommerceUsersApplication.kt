package com.dags.ecommerceusers

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class EcommerceUsersApplication

fun main(args: Array<String>) {
	runApplication<EcommerceUsersApplication>(*args)
}
