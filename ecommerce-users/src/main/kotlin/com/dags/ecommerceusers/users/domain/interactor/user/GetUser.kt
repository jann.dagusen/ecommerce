package com.dags.ecommerceusers.users.domain.interactor.user

import com.dags.ecommerceusers.users.domain.model.User

interface GetUser {
    fun execute(id: Long): User
}