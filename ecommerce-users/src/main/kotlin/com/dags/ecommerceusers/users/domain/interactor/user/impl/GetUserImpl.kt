package com.dags.ecommerceusers.users.domain.interactor.user.impl

import com.dags.ecommerceusers.users.domain.gateway.user.UserGateway
import com.dags.ecommerceusers.users.domain.interactor.user.GetUser
import com.dags.ecommerceusers.users.domain.model.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class GetUserImpl @Autowired constructor(val gateway: UserGateway) : GetUser {
    override fun execute(id: Long): User = gateway.find(id)
}