package com.dags.ecommerceusers.users.data.gateway.user

import com.dags.ecommerceusers.users.data.entity.UserEntity
import com.dags.ecommerceusers.users.data.mapper.user.UserDataMapper
import com.dags.ecommerceusers.users.data.repository.user.UserRepository
import com.dags.ecommerceusers.users.domain.gateway.user.UserGateway
import com.dags.ecommerceusers.users.domain.model.User
import com.dags.ecommerceusers.users.domain.param.user.UserParam
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

@Service
class UserGatewayImpl @Autowired constructor(val mapper: UserDataMapper,
                                             val repository: UserRepository,
                                             val passwordEncoder: PasswordEncoder) : UserGateway {
    override fun findAll(q: String?): List<User> = mapper.map(repository.findAll())

    override fun create(param: UserParam): User {
        return mapper.map(repository.save(
                UserEntity(
                        param.username,
                        passwordEncoder.encode(param.password))))
    }

    override fun find(id: Long): User = mapper.map(repository.findById(id).get())
}