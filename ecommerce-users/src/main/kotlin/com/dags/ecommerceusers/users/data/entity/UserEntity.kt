package com.dags.ecommerceusers.users.data.entity

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "users")
data class UserEntity(
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        val id: Long?,

        @Column(name = "username")
        val username: String,

        @Column(name = "password")
        val password: String
) {
    constructor(
            username: String,
            password: String)
            :
            this(null,
                    username,
                    password)
}