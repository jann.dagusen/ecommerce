package com.dags.ecommerceusers.users.domain.gateway.user

import com.dags.ecommerceusers.users.domain.model.User
import com.dags.ecommerceusers.users.domain.param.user.UserParam

interface UserGateway {
    fun findAll(q: String?): List<User>
    fun create(param: UserParam): User
    fun find(id: Long): User
}