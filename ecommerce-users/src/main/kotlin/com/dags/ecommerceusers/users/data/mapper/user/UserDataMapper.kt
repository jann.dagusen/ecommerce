package com.dags.ecommerceusers.users.data.mapper.user

import com.dags.ecommerceusers.users.data.entity.UserEntity
import com.dags.ecommerceusers.users.domain.model.User

interface UserDataMapper {
    fun map(entities: List<UserEntity>): List<User>
    fun map(entity: UserEntity): User
}