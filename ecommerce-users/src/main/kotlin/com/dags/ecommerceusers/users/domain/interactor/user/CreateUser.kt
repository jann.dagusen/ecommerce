package com.dags.ecommerceusers.users.domain.interactor.user

import com.dags.ecommerceusers.users.domain.model.User
import com.dags.ecommerceusers.users.domain.param.user.UserParam

interface CreateUser {
    fun execute(param: UserParam): User
}