package com.dags.ecommerceusers.api.mapper

import com.dags.ecommerceusers.api.request.UserForm
import com.dags.ecommerceusers.api.response.UserApiResp
import com.dags.ecommerceusers.users.domain.model.User
import com.dags.ecommerceusers.users.domain.param.user.UserParam

interface UserApiMapper{
    fun map(users: List<User>): List<UserApiResp>
    fun map(user: User): UserApiResp
    fun map(form: UserForm): UserParam
}