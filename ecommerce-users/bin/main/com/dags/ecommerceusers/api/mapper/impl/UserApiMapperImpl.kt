package com.dags.ecommerceusers.api.mapper.impl

import com.dags.ecommerceusers.api.mapper.UserApiMapper
import com.dags.ecommerceusers.api.request.UserForm
import com.dags.ecommerceusers.api.response.UserApiResp
import com.dags.ecommerceusers.users.domain.model.User
import com.dags.ecommerceusers.users.domain.param.user.UserParam
import org.springframework.stereotype.Service

@Service
class UserApiMapperImpl : UserApiMapper {
    override fun map(users: List<User>): List<UserApiResp> = users.map { map(it) }

    override fun map(user: User): UserApiResp = UserApiResp(
            user.id,
            user.username,
            user.password
    )

    override fun map(form: UserForm): UserParam = UserParam(
            form.username,
            form.password
    )
}