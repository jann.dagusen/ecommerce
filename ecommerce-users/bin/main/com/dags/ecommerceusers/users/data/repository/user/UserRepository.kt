package com.dags.ecommerceusers.users.data.repository.user

import com.dags.ecommerceusers.users.data.entity.UserEntity
import org.springframework.data.jpa.repository.JpaRepository

interface UserRepository : JpaRepository<UserEntity, Long> {
    fun findByUsername(username: String): UserEntity?
}