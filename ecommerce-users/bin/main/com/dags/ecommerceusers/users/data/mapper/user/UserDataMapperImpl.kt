package com.dags.ecommerceusers.users.data.mapper.user

import com.dags.ecommerceusers.users.data.entity.UserEntity
import com.dags.ecommerceusers.users.domain.model.User
import org.springframework.stereotype.Service

@Service
class UserDataMapperImpl : UserDataMapper {
    override fun map(entities: List<UserEntity>): List<User> = entities.map { map(it) }

    override fun map(entity: UserEntity): User = User(
            entity.id!!,
            entity.username,
            entity.password
    )
}