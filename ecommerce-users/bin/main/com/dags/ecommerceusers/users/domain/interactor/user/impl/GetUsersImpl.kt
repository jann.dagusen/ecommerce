package com.dags.ecommerceusers.users.domain.interactor.user.impl

import com.dags.ecommerceusers.users.domain.gateway.user.UserGateway
import com.dags.ecommerceusers.users.domain.interactor.user.GetUsers
import com.dags.ecommerceusers.users.domain.model.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class GetUsersImpl @Autowired constructor(val gateway: UserGateway) : GetUsers {
    override fun execute(q: String?): List<User> = gateway.findAll(q)
}