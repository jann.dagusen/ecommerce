package com.dags.ecommerceusers.users.domain.interactor.user

import com.dags.ecommerceusers.users.domain.model.User

interface GetUsers {
    fun execute(q: String?): List<User>
}