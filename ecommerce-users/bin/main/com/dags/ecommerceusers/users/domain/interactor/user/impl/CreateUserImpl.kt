package com.dags.ecommerceusers.users.domain.interactor.user.impl

import com.dags.ecommerceusers.users.domain.gateway.user.UserGateway
import com.dags.ecommerceusers.users.domain.interactor.user.CreateUser
import com.dags.ecommerceusers.users.domain.model.User
import com.dags.ecommerceusers.users.domain.param.user.UserParam
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class CreateUserImpl @Autowired constructor(val gateway: UserGateway) : CreateUser {
    override fun execute(param: UserParam): User {
        return gateway.create(param)
    }
}