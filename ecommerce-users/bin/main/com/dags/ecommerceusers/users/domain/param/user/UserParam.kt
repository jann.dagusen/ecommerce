package com.dags.ecommerceusers.users.domain.param.user

data class UserParam(
        val username: String,
        val password: String
)